import line, { Client, TextEventMessage, TemplateMessage } from '@line/bot-sdk';

export default class Bot {
  private static INSTANCE: Bot;
  public config: line.ClientConfig;
  private client: line.Client;

  public static init(): Bot {
    if (this.INSTANCE === undefined) this.INSTANCE = new Bot();
    return this.getInstance();
  }

  public static getInstance(): Bot {
    return this.INSTANCE;
  }

  constructor() {
    this.config = {
      channelAccessToken: process.env.channelAccessToken as string,
      channelSecret: process.env.channelSecret as string
    };
    this.client = new Client(this.config);
  }

  public test() {
    this.client.pushMessage(process.env.userID as string, {
      type: 'text',
      text: 'Bot set up! 😎'
    });
  }

  public async handle(event: line.WebhookEvent) {
    switch (event.type) {
      case 'message': {
        const msg = <TextEventMessage>event.message;
        if (msg.text === 'give me an img') {
          this.client.replyMessage(event.replyToken, {
            type: 'image',
            originalContentUrl: 'https://picsum.photos/200/300',
            previewImageUrl: 'https://picsum.photos/200/300'
          });
          break;
        } else if (msg.text === 'help') {
          this.client.replyMessage(event.replyToken, this.help);
          break;
        }
        this.client.replyMessage(event.replyToken, {
          type: 'text',
          text: `You say "${msg.text}" 👀`
        });
        break;
      }
      case 'follow': {
        this.client.replyMessage(event.replyToken, this.help);
        break;
      }
    }
  }

  private help: TemplateMessage = {
    type: 'template',
    altText: 'this is a confirm template',
    template: {
      type: 'buttons',
      text: 'Function',
      actions: [
        {
          type: 'message',
          label: '🖼️ Give me an image',
          text: 'give me an img'
        },
        {
          type: 'message',
          label: '🙋 Help !',
          text: 'help'
        },
        {
          type: 'message',
          label: '👀 Hi',
          text: 'Hi'
        }
      ]
    }
  };
}
