import express from 'express';
import { middleware, MiddlewareConfig, WebhookEvent } from '@line/bot-sdk';
import Bot from './bot';

export default class Server {
  private static INSTANCE: Server;
  private app: express.Application;
  private port: number = parseInt(<string>process.env.PORT, 10) || 3000;

  public static init(): Server {
    if (this.INSTANCE === undefined) this.INSTANCE = new Server();
    return this.getInstance();
  }

  constructor() {
    this.app = express();
  }

  public static getInstance(): Server {
    return this.INSTANCE;
  }

  public start() {
    this.app.use(
      '/webhook',
      middleware(Bot.getInstance().config as MiddlewareConfig)
    );
    this.routes();
    this.app.listen(this.port, (): void => {
      console.log(`Server start successfully on port ${this.port}`);
    });
  }

  private routes() {
    this.app.get('/', (req, res) => {
      res.sendStatus(200);
    });

    this.app.post('/webhook', (req, res) => {
      Promise.all(
        req.body.events.map((event: WebhookEvent) => {
          Bot.getInstance().handle(event);
        })
      ).then((result) => res.json(result));
    });
  }
}
